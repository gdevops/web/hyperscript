.. index::
   ! hyperscript

.. _hyperscript_def:

==================================================================================
**hyperscript definition**
==================================================================================

- https://hyperscript.org/comparison/

**hyperscript is a scripting language designed for modern front-end web development.**

hyperscript makes writing event handlers and highly responsive user
interfaces trivial with native language support for async behavior - easier
than promises or async/await.

hyperscript features include:

- Events as first class citizens in the language. Clean syntax for
  `responding to <https://hyperscript.org/features/on>`_ and sending events,
  as well as event-driven control flow
- DOM-oriented syntax with seamless integrated CSS id, CSS class and CSS query literals
- First class web workers
- An async-transparent runtime for highly responsive user experiences.
- A pluggable & extendable parser & grammar
- A debugger to step through hyperscript code

**hyperscript is a companion project of htmx**.

Because hyperscript relies on promises, it does not strive for IE11 compatibility.

You can see a comparison of hyperscript, vanillaJS and jQuery `here <https://hyperscript.org/comparison/>`_.
