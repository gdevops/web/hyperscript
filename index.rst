.. index::
   ! hyperscript

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

|FluxWeb| `RSS <https://gdevops.frama.io/web/hyperscript/rss.xml>`_


.. _hyperscript:

===================================================================================================
**hyperscript** an easy and approachable language designed for modern front-end web development
===================================================================================================

- https://github.com/bigskysoftware/_hyperscript
- https://github.com/bigskysoftware/_hyperscript/issues
- https://hyperscript.org/
- https://hyperscript.org/docs/
- https://hypermedia.systems/

.. figure:: images/hyperscript_logo.png
   :align: center

.. toctree::
   :maxdepth: 5

   definition/definition
   news/news

.. toctree::
   :maxdepth: 3

   versions/versions

