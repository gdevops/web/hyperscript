
.. _hyperscript_2021_12_01:

==================================================================================
**A First Look at _hyperscript**  by Ben Croker
==================================================================================

- https://putyourlightson.com/articles/a-first-look-at-hyperscript#scripting-not-programming


Introduction
===============

​The `hyperscript <https://hyperscript.org/>`_ is a **scripting language**
for adding interactivity to the front-end.

You guessed it, yet another JavaScript library. But what makes hyperscript
unique is that it provides a natural language syntax for optimal readability,
reusability and maintainability, arguably the most important qualities
of any ​**programming** language

Let’s begin by taking a look at some hyperscript::

    <button _="on click add .hidden">
        ...
    </button>

The underscore (_) attribute is where you put hyperscript by default,
although you can also use a script or data-script attribute, or even
create your own custom attribute name.

The hyperscript above can be translated as follows:

When the button element is clicked, add the class hidden to it.
